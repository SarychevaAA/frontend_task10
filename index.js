const personalMovieDB = {
    count: 0,
    movies: {},
    actors: {},
    genres: [],
    privat: false,
    start: function (){
        personalMovieDB.count = +prompt('Сколько фильмов вы уже посмотрели?', '0');
    },
    writeYourGenres: function (){
        let genreQuestions = 0;
        while (genreQuestions < 3){
            let loveGenre = prompt( `Ваш любимый жанр под номером ${genreQuestions+1}`, '');
            if (loveGenre == null || loveGenre === ''){
                alert('Вы не ответили на вопрос');
            }
            else{
                personalMovieDB.genres.push(loveGenre);
                genreQuestions++;
            }
        }
        personalMovieDB.genres.forEach((item, index)=>{
            console.log(`Любимый жанр ${index+1} - это ${item}`)
        })
    },
    showMyDB: function (){
        if (personalMovieDB.privat === false){
            console.log(personalMovieDB);
        }
    },
    saveFilmInfo: function (){
        let countQuestions = 0
        while (countQuestions < 2){
            let lastSeeFilm = prompt( 'Один из последних просмотренных фильмов?', '')
            let filmRating = +prompt('На сколько оцените его?', '')
            if (lastSeeFilm.length < 50){
                alert('Название фильма короче 50 символов, попробуйте еще раз');
            }
            else{
                personalMovieDB.movies[lastSeeFilm] = filmRating;
                countQuestions++;
            }
        }
    },
    determineLevelPerson: function (){
        if (personalMovieDB.count < 10 && personalMovieDB.count >= 0){
            alert('Просмотрено довольно мало фильмов');
        }
        else if (personalMovieDB.count <= 30 && personalMovieDB.count >= 0){
            alert('Вы классический зритель');
        }
        else if (personalMovieDB.count > 30){
            alert('Вы киноман');
        }
        else{
            alert('Произошла ошибка');
        }
    },
    toggleVisibleMyDB: function (){
        personalMovieDB.privat = !personalMovieDB.privat;
    }
}
